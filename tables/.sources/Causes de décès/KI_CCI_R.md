---
permalink: /tables/KI_CCI_R
---
# KI\_CCI\_R
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "tables/.schemas/Causes de décès/KI_CCI_R.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
