---
permalink: /tables/KI_ECD_R
---
# KI\_ECD\_R
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "tables/.schemas/Causes de décès/KI_ECD_R.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
