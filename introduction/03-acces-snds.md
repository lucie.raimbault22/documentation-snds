# Comment accéder au SNDS ?
<!-- SPDX-License-Identifier: MPL-2.0 -->

En dehors de l'Open Data, présenté plus haut, deux types distincts d'accès aux données du SNDS sont possibles : les accès permanents, et les accès sur projet.

## Les accès permanents

Certains organismes chargés d'une mission de service public, [listés par décret](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033702840&categorieLien=id), disposent d'un accès permanent aux données du SNDS.
Cette autorisation pérenne concerne un périmètre spécifique des données, et se décline ainsi selon les niveaux d'agrégation (données individuelles/agrégées bénéficiaires, …) et les historiques accessibles.

## Les accès sur projet

Pour tous les demandeurs, publics comme privés, la loi prévoit la possibilité de demander un accès à une extraction de données du SNDS pour un projet d'intérêt public à des fins de recherche, d'étude et d'évaluation dans le domaine de la santé.
Selon les cas, cette demande d'accès peut relever de différentes procédures.

### La procédure standard

La procédure standard d'accès au SNDS, la plus courante, suit les étapes suivantes :

1. Le Responsable de Traitement (RT) dépose une demande d'autorisation auprès du Health Data Hub, qui valide la complétude du dossier, et peut s'auto-saisir pour examiner l'intérêt public du traitement.  Le Health Data Hub doit adresser sous 7 jours ouvrés le dossier dans sa complétude au CESREES.
2. Le Comité Éthique et Scientifique pour les Recherches, les Études et les Évaluations dans le domaine de la santé ([CESREES](https://www.health-data-hub.fr/cesrees)) émet un avis sur la méthodologie scientifique du projet, et le recours à des données à caractère personnel (délai : 1 mois)
3. La Commission Nationale de l'Informatique et des Libertés ([CNIL](../glossaire/CNIL.md)) prend connaissance des avis du CESREES, et donne une autorisation pour le projet (délai : 2 mois, renouvelable une fois)
4. A l'issue de l'autorisation [CNIL](../glossaire/CNIL.md) et après signature d'une convention, les données SNDS nécessaires à l'étude pourront être traitées dans un espace projet dédié (délai indicatif de mise à disposition des données : 2 mois)

:::tip Pour plus d'informations
Vous pouvez consulter le [Kit de démarrage avec les données de santé](https://documentation-snds.health-data-hub.fr/ressources/starter_kit.html#de-quelles-donnees-ai-je-besoin-pour-mon-projet)
:::

### Les méthodologies de référence

Les méthodologies de référence (MR) sont des procédures simplifiées d'accès aux données, qui permettent dans certaines situations de réaliser une recherche en santé sans nécessiter d'autorisation [CNIL](../glossaire/CNIL.md) ou d'avis du CESREES.
Le Responsable de Traitement adresse alors à la [CNIL](../glossaire/CNIL.md) une déclaration attestant la conformité du projet à la MR, puis inscrit son traitement dans le [répertoire public](https://www.indsante.fr/fr/repertoire-public-des-etudes-realisees-sous-mr).

### La procédure d'accès simplifié à l'[EGB](../glossaire/EGB.md)

La [CNIL](../glossaire/CNIL.md) a homologué en 2018 une procédure d'accès simplifié à l'[EGB](../glossaire/EGB.md), en sa qualité d'échantillon représentatif.
La [CNIL](../glossaire/CNIL.md) donne ainsi compétence à l'INDS pour approuver (délai : 15 jours) l'accès à l'[EGB](../glossaire/EGB.md) après examen des 5 conditions suivantes :

- la finalité d'intérêt public du projet
- la justification par le RT de la pertinence scientifique du projet
- l'absence de croisements d'identifiants potentiels
- la durée d'accès aux données qui doit être limitée à celle nécessaire à la réalisation du projet
- le respect du référentiel de sécurité SNDS

:::warning Attention
La mission d’examen du caractère d’intérêt public de la finalité poursuivie relève dorénavant du CESREES (créé par le décret du 14 mai 2020). 
La procédure d'accès simplifié ne peut donc être maintenue en l’état. 
D’ores et déjà une procédure amendée a été soumise à la CNIL et sera proposée dès adoption par cette dernière.  
:::

### Les assureurs et industriels de santé

L'accès des industriels de santé (entreprises productrices de produits de santé) et des assureurs en santé au SNDS est plus fortement encadré.
Ces acteurs doivent :

- soit passer par un bureau d'études ou un organisme de recherche indépendant
- soit démontrer l'impossibilité d'utiliser le SNDS pour des finalités interdites
