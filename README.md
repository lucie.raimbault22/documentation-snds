# Documentation collaborative du SNDS
<!-- SPDX-License-Identifier: MPL-2.0 -->

Bienvenue sur la documentation collaborative du Système National des Données de Santé.

Cette documentation est en construction, via [ce dépôt GitLab](https://gitlab.com/healthdatahub/documentation-snds).

::: danger Attention
Internet Explorer cause des erreurs de navigation sur ce site, et sur GitLab. 

Nous conseillons d'utiliser un autre navigateur, car Microsoft a arrêté le développement d'Internet Explorer depuis quelques années.
:::

## Contributeurs 

Cette documentation est maintenue par le Health data hub.

Elle résulte d'une mise en commun de documents et travaux par plusieurs organisations, dont :
- la Caisse nationale d'assurance maladie - [Cnam](https://www.ameli.fr/)
- le Health Data Hub - [HDH](https://www.health-data-hub.fr)
- Le Ministère des Solidarités et de la Santé: la Direction de la Recherche, des études, de l’évaluation et des statistiques - 
[DREES](https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/la-drees/) 
et la Direction de la Sécurité Sociale [DSS](https://www.securite-sociale.fr/la-secu-cest-quoi/organisation/la-direction-de-la-securite-sociale)
- les Agences régionales de santé - [ARS](https://www.ars.sante.fr/)
- Santé publique France - [SpF](https://www.santepubliquefrance.fr/)


## Organisation

La documentation est organisée en 5 sections :
- [Introduction](introduction/README.md) est un guide introductif au SNDS ;
- [Fiches thématiques](fiches/README.md) contient des fiches thématiques détaillées ;
- [Glossaire](glossaire/README.md) contient des fiches explicitant des concepts importants, utilisées comme références ailleurs ;
- [Ressources](ressources/README.md) liste de nombreuses ressources externes ou à télécharger ;
- [Tables](tables/README.md) est un dictionnaire des tables et variables ;
- [Contribuer](contribuer/README.md) est un guide de contribution à cette documentation.

Chacune de ces sections correspond à un dossier sur [GitLab](https://gitlab.com/healthdatahub/documentation-snds), avec un [dossier supplémentaire](https://gitlab.com/healthdatahub/documentation-snds/files) pour les fichiers et images.

## Citation

Merci de citer ce site si vous l'utilisez dans vos travaux.

::: tip Citation au format recommandé par l'APA
> Documentation collaborative du SNDS. (n.d.). Retrieved from https://gitlab.com/healthdatahub/documentation-snds 
:::

## Licence

Ce dépôt est publié par le Health data hub sous 
licence Mozilla Public License 2.0 (MPL-2.0)

Voir le fichier [LICENSE](https://gitlab.com/healthdatahub/documentation-snds/blob/master/LICENSE).
